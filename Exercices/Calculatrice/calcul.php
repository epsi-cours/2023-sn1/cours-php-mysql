<?php

$number1 = $_POST['number1'];
$number2 = $_POST['number2'];
$operation = $_POST['operation'];

switch ($operation) {
    case '+':
        $result = $number1 + $number2;
        break;
    case '-':
        $result = $number1 - $number2;
        break;
    case '*':
        $result = $number1 * $number2;
        break;
    case '/':
        if ($number2 != 0) {
            $result = $number1 / $number2;
        } else {
            $error = 'Vous ne pouvez pas diviser par 0';
        }
        break;
    default:
        $error = 'Opération non reconnue : '.$operation;
}

require_once('result.html.php');