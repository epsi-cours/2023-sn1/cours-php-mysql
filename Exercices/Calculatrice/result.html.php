<html lang="en" data-bs-theme="dark">
<head>
    <title>Calculatrice</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<main>
    <div class="px-4 py-5 my-5 text-center">
        <h1 class="display-5 fw-bold text-body-emphasis">Résultat du calcul !</h1>
        <div class="col-lg-6 mx-auto">
            <?php if (isset($error)) {
                echo $error;
            } else if (isset($result)) {
                echo 'Le resultat de '.$number1.' '.$operation.' '.$number2.' est '.$result;
            } ?>
        </div>
    </div>
</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>