<?php

function add(int $number1, int $number2): int {
    $result = $number1 + $number2;

    return $result;
}

function addArray(array $numbers): int {
    if (count($numbers) == 1 && is_int($numbers[0])) {
        return $numbers[0];
    }

    if (count($numbers) > 1) {
        $result = 0;
        foreach ($numbers as $number) {
            if(is_int($number)) {
                $result += $number;
            }
        }
        return $result;
    }

    return 0;
}

function sortArray(array $numbers): array {
    $size = count($numbers);
    for ($i=0; $i < $size - 1; $i++) {
        if($numbers[$i] > $numbers[$i+1]) {
            $temp = $numbers[$i+1];
            $numbers[$i+1] = $numbers[$i];
            $numbers[$i] = $temp;
            // $numbers[$i] = min($numbers[$i], $numbers[$i+1]);
            // $numbers[$i+1] = max($numbers[$i], $numbers[$i+1]);
        }
    }
    if ($size > 2) {
        $biggerNumber = $numbers[$size-1];
        $firstPart = $numbers;
        unset($firstPart[$size-1]);
        $firstPartSorted = sortArray($firstPart);
        $numbers = $firstPartSorted;
        $numbers[] = $biggerNumber;
    }

    return $numbers;
}