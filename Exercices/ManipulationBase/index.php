<?php
require_once('functions.php');

# Les variables
$firstname = 'John';
$lastname = 'Doe';

// La concatenation : coller deux shaines de caractère
//* echo 'Mon prénom est '.$firstname.' '.$lastname."\n";  // Voici un commentaire sur une ligne
/* Avec des doubles quote, php scanne le contenu pour
 * remplacer les valeurs d'éventuelle variable contenues dans la chaine
 */
// echo "Mon prénom est $firstname $lastname\n";

// Calculs mathématiques
$hiking1 = 12; //kms
$hiking2 = 15; //kms
$totalHiking = add($hiking1, $hiking2);

/* echo 'La longueur de la balade 1 est de '.$hiking1
    .', la balade 2 fait '.$hiking2.' donc la longueur totale est de '
    .$totalHiking."\n";/**/

// Multiplication
$grade = 12;
$coefficient = 1.5;

$points = $grade * $coefficient;

/* echo 'Sachant que la note est '.$grade.
    ' et le coefficient '.$coefficient.
    ' le nombre de points gagnés est '.$points."\n";/**/

$points = 25;
$coefficient = 3;

$grade = $points / $coefficient;


/* echo 'Sachant que le nombre de points est '.$points.
    ' et le coefficient '.$coefficient.
    ' la note initiale est '.$grade."\n";/**/

// Division euclidienne
$points = 25;
$coefficient = 3;
$rest = $points % $coefficient;
$result = ($points - $rest) / $coefficient;

echo 'La division euclienne de '.$points.' par ' .$coefficient.' est '
    .$points.' = '.$coefficient.' * '.$result.' + '.$rest."\n";

// Le type tableau
$sports = ['foot', 'volley', 'bad', 'hand'];

/*echo 'La liste des sports est :';
foreach ($sports as $sport) {
    echo ' '.$sport;
}
echo "\n";/**/

$sportsAsString = implode(', ', $sports); // Concatenation du tableau en une chaine de caractère
// echo 'La liste des sports est : '.$sportsAsString."\n";

$newSport = 'rugby';
// ajout dans la tableau
$sports[] = $newSport;

/*echo 'La liste des sports est :';
foreach ($sports as $key => $sport) {
    echo ' '.$sport;
    echo ' '.$sports[$key];
}
echo "\n";/**/

// print_r($sports);

// Boucle for
// Afficher tous les elements du tableau avec une boucle for
for ($i=0; $i<=4; $i = $i+1) {
    echo ' '.$sports[$i];
}

$boat = [
    'name' => 'Nautilus',
    'length' => 12,
    'width' => 3,
    'depth' => 1.5,
];

// print_r($boat);

foreach ($boat as $property => $value) {
    echo $property.' : '.$boat[$property]."\n";

}


/*
 * Exercice de tri d'un tableau
 */

$initialArray = [5, 3, 7, 1, 9, 2, 8, 4, 6];
print_r($initialArray);

$sortedArray = sortArray($initialArray);
print_r($sortedArray);