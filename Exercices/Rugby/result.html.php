<html lang="en" data-bs-theme="dark">
    <head>
        <title>Rugby</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <main>
            <div class="px-4 py-5 my-5 text-center">
                <h1 class="display-5 fw-bold text-body-emphasis">Resultat</h1>
                <div class="col-lg-6 mx-auto">
                    <p class="lead mb-4">Le résultat du match entre <?php echo $team1; ?> et <?php echo $team2; ?> est <?php echo $winnerScore; ?> / <?php echo $loserScore; ?> pour l'équipe <?php echo $winnerName; ?></p>
                </div>
            </div>
        </main>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>