<?php

//echo 'variables POST';
//print_r($_POST);
//echo '<br />';

$team1 = $_POST['team1'];
$team2 = $_POST['team2'];
$score1 = $_POST['score1'];
$score2 = $_POST['score2'];
$color1 = $_POST['color1'];
$color2 = $_POST['color2'];

if ($score1 > $score2) {
    $draw = false;
    $winnerName = $team1;
    $winnerScore = $score1;
    $winnerColor = $color1;

    $loserName = $team2;
    $loserScore = $score2;
    $loserColor = $color2;
} elseif ($score2 > $score1) {
    $draw = false;
    $winnerName = $team2;
    $winnerScore = $score2;
    $winnerColor = $color2;

    $loserName = $team1;
    $loserScore = $score1;
    $loserColor = $color1;
} else {
    $draw = true;
    $drawScore = $score1;
}

/*if ($draw === false) {
    echo 'L\'équipe gagnante est '.$winnerName. ', elle a gagné par le score de '.$winnerScore
        .' à '.$loserScore.' l\'équipe '.$loserName;
} else {
    echo 'Il y a eu égalité entre les équipes '.$team1.' et '.$team2
        .' sur le score de '.$drawScore.' partout.';
}/**/

require_once('result.html.php');