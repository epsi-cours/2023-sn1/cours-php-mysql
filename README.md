# Cours sur PHP et MySQL

## Introduction

Qu'est-ce qu'un langage de programmation ?
Qu'est-ce qu'une base de données ?

## Le langage PHP

> PHP est le langage de programmation côté serveur le plus utilisé sur le web. En fait, 79,2 % de tous les sites web reposent sur PHP à un degré ou à un autre, ce qui en fait l’un des langages les plus populaires parmi les programmeurs et les développeurs web en raison de son utilisation généralisée.
> 
> <cite>Source : [kingsta.com][1]</cite>
> 
[1]: https://kinsta.com/fr/part-de-marche-php

- [Utilisation de PHP chez les développeurs](https://www.jetbrains.com/fr-fr/lp/devecosystem-2022/php/)
- [Documentation officielle de PHP](https://www.php.net)

Langage en constante évolution : [Versions de PHP](https://www.php.net/supported-versions.php)

Installer son environnement

### Pré requis :
- Avoir PHP installé sur son poste en version 8.0 ou supérieure
- Avoir une base de données MySQL installée sur son poste

Sur windows
> XAMPP Server peut être une solution pour ces deux points

Sur Mac :

https://brew.sh
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew install php
brew install mysql
```

Editeur de code :
- PHPStorm, gratuit pendant 30 jours : https://www.jetbrains.com/fr-fr/phpstorm/download/#section=mac
- Visual studio code, open source : https://code.visualstudio.com/download


### Principes de fonctionnement
Le modèle client server

 ![image](img/client-server.png)

Le fonctionnement de PHP et MySQL sur le server

![image](img/php-mysql.png)


### Les variables
```php
$variable = valeur;
```

### Les types de données
[Documentation](https://www.php.net/manual/fr/language.types.php)

Le type string
```php
$firstname = 'John';
$lastname = 'Doe';

$name = "$firstname $lastname";
echo $name.' a '.$age.' ans';
```

Le type integer
```php
$age = 18;
```

Le type boolean 
```php
$adult = true;
```

Le type tableau
```php
$fruits = ['Pomme', 'Banane', 'Kiwi'];

if (in_array('Pastèque')) {
    echo 'Pastèque fait partie de ma liste de fruits';
} else {
    echo 'Je n\'ai pas de pastèque';
}

$nbFruits = count($fruits);
echo 'Il y a '.$nbFruits.' fruits<br>';

$fruits[] = 'Pastèque';
echo 'Il y a '.count($fruits).' fruits<br>';

foreach ($fruits as $fruit) {
    echo 'Le tableau contient des '.$fruit.'<br>';
}

foreach ($fruits as $key => $fruit) {
    echo 'Le tableau contient des '.$fruit.' à la position '.$key.'<br>';
}

echo 'A la place numéro 2 il y a '.$fruits[2].'<br>';
```


Le type date
```php
$date = new Datetime;

echo 'Aujourd\'hui nous sommes le ' . $date->format('d/m/Y') . ' et il est '.$date->format('H:i');
```

### Les opérateurs
Les opérateurs permetent de tester une valeur ou attribuer une valeur dans une variable

`=` permet d'attribuer une valeur dans une variable
```php
$firstname = 'John';
```

`==` permet de tester une valeur
```php
if ($color == 'yellow') {
    echo 'La couleur est jaune';
}
```

`===` permet de tester une valeur et un type de donnée
```php
$age = '18';
if ($age == 18) { // true
    echo 'Age vaut 18';
}
if ($age === 18) { // false
    echo 'Age vaut 18, de type entier';
}
```

`!=` Permet de tester la différence
```php
$firstname = 'John';
if ($firstname != 'Robert') {
    echo 'Le prénom n\'est pas Robert';
}
```

`!==` Permet de tester la différence de valeur et de type
```php
$age = '18';

if ($age !== 18) { // false
    echo 'Age ne vaut pas 18, ou n\'est pas de type entier';
}
```

`&&` Permet de vérifier deux tests à la fois (c'est le "et" logique)
```php
$firstanme = 'John';
$lastname = 'Doe';

if ($firstanme == 'John' && $lastname == 'Doe') {
    echo 'Houston ! Houston ! Ici John Doe, me recevez vous ?';
}
```

`||` Permet de vérifier un test ou un autre (c'est le "ou" logique)
```php
if ($color == 'yellow' || $color == 'blue') {
    echo 'La couleur est jaune ou bleu';
}
```

`.=` Concaténer des chines de caractère
```php
$name = 'John';
$name .= ' ';
$name .= 'Doe';

echo $name; // John Doe
```

`++` Incrémenter une variable de 1
```php
$index = 20;
$index++;

echo $index; // 21
```

`--` Décrémenter une variable de 1
```php
$index = 25;
$index--;

echo $index; // 24
```

`+=` Incrémenter une variable d'un nombre précis
```php
$index = 42;
$index += 10;

echo $index; // 52
```

`-=` Décrémenter une variable d'un nombre précis
```php
$index = 20;
$index += 3;

echo $index; // 17
```

### Les instructions conditionnelles
```php
if ($age >= 18) {
    echo 'Cette persone est majeure';
} else {
    echo 'Cette personne est mineure'
}
```

### Les boucles

For
```php
echo 'Compter de 1 à 10 :<br>';
for ($i = 0 ; $i <= 10 ; $i = $i + 1) {
    echo $i;
}
```
Compter de 1 à 10 avec uniquement les numéros paires
1ère méthode
```php
for ($i = 0 ; $i <= 10 ; $i = $i + 1) {
    if (($i % 2) == 0) {
        echo $i;
    }
}
```
2ème méthode
```php
for ($i = 0 ; $i <= 10 ; $i = $i + 2) {
    echo $i;
}
```
3ème méthode
```php
for ($i = 0 ; $i <= 5 ; $i = $i + 1) {
    echo 2*$i;
}
```

Boucle foreach
```php
$fruits = ['Pommes', 'Bananes', 'Kiwis'];

foreach ($fruits as $fruit) {
    echo "J'ai des $fruit\n";
}
```

Boucle while
```php
$i = 1;
while ($i <= 10) {
$i = $i+1;
    echo $i;
}
```

Exercice : Trier un tableau par ordre alphabétique


## Les fonctions

```php
function add($numbers) {
    $result = 0;

    foreach ($numbers as $number) {
        $result = $result + $number;
    }
    
    return $result;
}

$numbers = [3, 6, 9, 8, 5, 6, 2];

echo 'La somme des nombre du tableau est : '.add($numbers);
```

Exercice : Faire une fonction pour trier le tableau


## Les bases de données

Utilité des bases de données
Présentation de PHPMyAdmin
Construction d’une base avec PHPMyAdmin
SQL sous PHPMyAdmin :
- Les principales instructions
- Lecture de code SQL
- Génération de multiples requêtes
- Les bonnes habitudes : tester ses requêtes avec PHPMyAdmin
  Fonctions d’accès aux bases de données
  Formulation de requêtes dans un fichier PHP